// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// This allows us to use all the routes define in "userRoute.js"
const userRoute = require("./routes/userRoute");

// This allows us to use all the routes define in "courseRoute.js"
const courseRoute = require("./routes/courseRoute");

// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoute);
app.use("/courses", courseRoute);

// Database Connection
mongoose.connect("mongodb+srv://johnlenkeltsijera:admin123@zuitt-bootcamp.qihxtqs.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.once("opne", () => console.log("Now connected to cloud database!"));

// Server listening
// Will used the defined port number for the application whenever environment variable is available to used port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as hosted application
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));




